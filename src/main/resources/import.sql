INSERT INTO players (id, login, password, email, birthday, full_name, gender, registration_time, avatar_path, enabled, role) VALUES
  (101, 'Vasya', 'nQm8kYsL4/FMpoTpcf30xHVIN7wj9+oO08BQHWbkRLQe+xyFJ4rIdhORffZNqYvG', 'Vasya@gmail.com', '12-12-1990', 'Vasiliy Smhidt', 'MALE', '28-11-2016 15:46:36', 'http://localhost:8080/resources/avatars/drake.jpg', true, 'ROLE_ADMIN');
INSERT INTO players (id, login, password, email, birthday, full_name, gender, registration_time, avatar_path, enabled, role) VALUES
  (102, 'Zhora', 'nQm8kYsL4/FMpoTpcf30xHVIN7wj9+oO08BQHWbkRLQe+xyFJ4rIdhORffZNqYvG', 'Zhora@gmail.com', '12-12-1990', 'Zhora Fridman', 'MALE', '28-11-2016 15:46:36', 'http://localhost:8080/resources/avatars/em.jpg', true, 'ROLE_PLAYER');
INSERT INTO players (id, login, password, email, birthday, full_name, gender, registration_time, avatar_path, enabled, role) VALUES
  (103, 'Kolya', 'nQm8kYsL4/FMpoTpcf30xHVIN7wj9+oO08BQHWbkRLQe+xyFJ4rIdhORffZNqYvG', 'Kolya@gmail.com', '12-12-1990', 'Mikalai Best', 'MALE', '28-11-2016 15:46:36', 'http://localhost:8080/resources/avatars/kanye.jpg', true, 'ROLE_PLAYER');
INSERT INTO players (id, login, password, email, birthday, full_name, gender, registration_time, avatar_path, enabled, role) VALUES
  (104, 'Petya', 'nQm8kYsL4/FMpoTpcf30xHVIN7wj9+oO08BQHWbkRLQe+xyFJ4rIdhORffZNqYvG', 'Petya@gmail.com', '12-12-1990', 'Petro Petro', 'MALE', '28-11-2016 15:46:36', 'http://localhost:8080/resources/avatars/lil.jpg', true, 'ROLE_PLAYER');
INSERT INTO players (id, login, password, email, birthday, full_name, gender, registration_time, avatar_path, enabled, role) VALUES
  (105, 'Grisha', 'nQm8kYsL4/FMpoTpcf30xHVIN7wj9+oO08BQHWbkRLQe+xyFJ4rIdhORffZNqYvG', 'Grisha@gmail.com', '12-12-1990', 'Grigoriy Matters', 'MALE', '28-11-2016 15:46:36', 'http://localhost:8080/resources/avatars/may.jpg', true, 'ROLE_PLAYER');

INSERT INTO games_history(id, gamestate, gametype, first_player_id, second_player_id) VALUES (101, 'FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER', 'HALMA', 101, 102);
INSERT INTO games_history(id, gamestate, gametype, first_player_id, second_player_id) VALUES (102, 'FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER', 'HALMA', 101, 102);
INSERT INTO games_history(id, gamestate, gametype, first_player_id, second_player_id)VALUES (103, 'FINISHED_WITH_DRAW', 'HALMA', 101, 102);
