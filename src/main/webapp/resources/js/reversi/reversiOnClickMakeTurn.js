/**
 * Provides methods for making turn by clicking on board's cell for {@link Reversi} game
 * on reversiGamePage.jsp
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

'use strict';

function makeTurn(currentId) {

    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: $('#idValue').val(), turn: $('#turn').val(parseId(currentId)).val()},
        success: function (data) {
            refreshField();
        },
        error: function () {
            console.log("bad connection to server");
        }
    });

}


function parseId(currentId) {
    var x, y, array;
    var stringX, stringY, turn;

    array = currentId.split("_");
    x = array[1] / 50;

    y = array[2] / 50;
    stringX = getXbyId(x);
    stringY = getYbyId(y);

    turn = stringX + stringY;

    return turn;

}

function getXbyId(temp) {
    var strX, character;

    if (temp >= 0 && temp < 8) {
        //from 65 to 72 unicode (letters from 'a' to 'h')

        for (var x = 0; x < 8; x++) {
            if (x == temp) {
                character = x + 65;
                strX = String.fromCharCode(parseInt(character));
            }
        }
    }

    return strX;
}

function getYbyId(temp) {
    var strY;

    if (temp >= 0 && temp < 8) {
        //from 1 to 8

        for (var x = 0; x < 8; x++) {
            if (x == temp) {
                strY = 8 - x;
            }
        }
    }

    return strY;
}