/**
 * Refresh field for {@link Reversi} game on reversiGamePage.jsp
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */
'use strict';

function refreshField() {

    $.ajax({
        type: "GET",
        url: 'ajax/board',
        data: 'gameId=' + $('#idValue').val(),
        dataType: 'text',
        success: function (responseText) {
            displayTips();
            clearField();
            drawField();
            syncField(responseText);
            refreshResultCode();
        },
        error: function (responseText) {
            console.log("bad connection to server");
        }
    });

}
