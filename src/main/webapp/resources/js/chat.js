$(document).ready(function () {
    connect();
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
$(window).bind("beforeunload", function() {
    return closeChat();
});


var stompClient = null;

function connect() {
    var socket = new SockJS('/chat-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/chat/getMessage', function (message) {
            var obj = JSON.parse(message.body);
            if (obj.player == $('#player').val()) {
                $('.chatbox__messages__user-message').append('<div class="chatbox__messages__user-message--ind-message-right"><table><tr><td><p class="name">' + obj.player + '</p><br/> ' +
                    '<p class="message">' + obj.text + '</p></td> ' +
                    '<td><img src="' + obj.avatar + '" width="50" height="50"/></td> </tr> </table></div>');
            } else {
                $('.chatbox__messages__user-message').append('<div class="chatbox__messages__user-message--ind-message-left"><table><tr>' +
                    '<td><img src="' + obj.avatar + '" width="50" height="50"/></td> ' +
                    '<td><p class="name">' + obj.player + '</p> <br/>' +
                    '<p class="message">' + obj.text + '</p> ' +
                    '</td></tr></table></div>');
            }
        });
        stompClient.subscribe('/chat/addAndGetPlayers', function (players) {
            showPlayers(players);
        });
        stompClient.subscribe('/chat/removeAndGetPlayers', function (players) {
            showPlayers(players);
        });
        stompClient.send("/add", {}, $('#player').val());
    });
}
function sendMessage(){
    var msg = JSON.stringify(
        {
            "player": $('#player').val(),
            "text": $('#text').val(),
            "avatar": $('#avatar').val()
        }
    );
    stompClient.send("/send", {}, msg);
    $('#text').val('');
}
function closeChat(){
    stompClient.send("/remove", {}, $('#player').val());
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function showPlayers(players){
    $('#user-list').empty();
    $('#user-list').append('<h1>Online</h1>');
    var arr = JSON.parse(players.body);
    arr.forEach(function(player, i, arr){
        $('#user-list').append('<div class="chatbox__user--active"><p>'+player+'</p></div>');
    });
}