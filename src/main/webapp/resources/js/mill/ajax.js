$(document).ready(function () {
    makeBoardClickableAndDroppable();
    displayTips();
    refreshBoard();
    setInterval(function () {
        displayTips();
        refreshBoard();
    }, 5000);
});

function refreshBoard() {
    console.log("in refreshBoard");
    $.ajax({
        method: 'GET',
        url: 'ajax/board?gameId=' + $('#gameId').val(),
        success: function (boardDTO) {
            drawBoard(boardDTO);
        }
    });
}

function displayTips() {
    $.ajax({
        method: 'GET',
        url: 'ajax/tips?gameId=' + $('#gameId').val(),
        success: function (tips) {
            if (tips.message == 'GAME OVER') {
                window.location.replace('finish?gameId=' + $('#gameId').val());
            }
            $('#tips').html(tips.message)
        }
    });
}

function drawBoard(boardDTO) {

    if (boardDTO == null) {
        return;
    }

    var board = boardDTO.board;
    var countOfWhite = boardDTO.countOfWhite;
    var countOfBlack = boardDTO.countOfBlack;

    for (var i = 0; i < 24; i++) {
        if (board[i] != 'none') {
            $('#' + i).html('<div  id="' + "s" + i + '" class="stone ' + board[i] + '"></div>');
        } else {
            $('#' + i).html("");
        }
    }

    makeStoneDraggable();

    $('#black').html('');
    $('#white').html('');
    while (countOfBlack > 0) {
        $('#black').append('<div class="blackStone"></div>');
        countOfBlack--;
    }
    while (countOfWhite > 0) {
        $('#white').append('<div class="whiteStone"></div>');
        countOfWhite--;
    }
}

function makeStoneDraggable() {
    $('.stone').draggable({
        containment: $('#board'),
        revertDuration: 0
    });
    console.log('make white draggable');
}

function makeBoardClickableAndDroppable() {
    $('.position').droppable({
        drop: function (event, ui) {
            var draggedId = ui.draggable.parent().attr('id');
            var droppedOnId = $(this).attr('id');
            makeTurnViaAjax(draggedId, droppedOnId);
        }
    });

    $('.position').click(function () {
        console.log('click success');
        makeTurnViaAjax($(this).attr('id'));
    });
}

function makeTurnViaAjax(p1, p2) {
    var gameId = $('#gameId').attr('value');
    var turn;

    if (p1 == undefined) {
        return;
    }

    if (p2 == undefined) {
        turn = p1;
    } else {
        turn = p1 + "," + p2;
    }

    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: gameId, turn: turn},
        success: function (data) {
            console.log('make turn success: ' + turn);
            displayTips();
            refreshBoard();
        },
        error: function () {
            console.log('make turn error');
        }
    });
}
