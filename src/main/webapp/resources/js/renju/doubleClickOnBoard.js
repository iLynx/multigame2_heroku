window.addEventListener('dblclick', ondblclick, false);

function ondblclick() {
    $('#board').dblclick(function (e) {
        var gameId = document.getElementById('gameId').value;
        var xClick = e.pageX - $(this).offset().left;
        var yClick = e.pageY - $(this).offset().top;
        var leftBorder = 48;
        var cellWidth = 32;
        var topBorder = 18;
        var cellHeight = 32;
        var boardCol = Math.floor((xClick - leftBorder) / cellWidth);
        var boardRow = Math.floor((yClick - topBorder) / cellHeight);
        var turn = boardRow + "," + boardCol;

        makeTurnViaAjax(gameId, turn);

        refreshBoard();
        refreshPlayerTurn();
    });
}

function makeTurnViaAjax(gameId, turn) {
    $.ajax({
        type: "POST",
        url: 'ajax/maketurn',
        data: {gameId: gameId, turn: turn},
        success: function (data) {
            refreshBoard();
            refreshPlayerTurn();
        },
        error: function () {
        	console.log('Make turn error');
        }
    });
}
