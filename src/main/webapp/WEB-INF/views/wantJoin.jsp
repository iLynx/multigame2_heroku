<%--/**
* UI for Want to join page
*
* @author Andrew Volyk andredewoll@gmail.com avolyk1tc
*/--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Want to play</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>

<body>

<div class="w3-container w3-center">
    <img alt="rspls.png" src="${pageContext.request.contextPath}/resources/img/multiLogo.png">
</div>

<spring:message code="label.wantJoin.game.message" var="gameMessage"/>
<spring:message code="label.wantJoin.ready.message" var="readyMessage"/>

<h2 style="text-align:center" class="w3-center w3-animate-left"> ${gameMessage} ${gameName}!</h2>
<h2 style="text-align:center" class="w3-center w3-animate-right"> ${readyMessage} ${gameOwner.login}?</h2>
<br>

<div class="w3-container w3-center" style="width:300px; margin:auto">
    <a class="btn btn-success" href="join?gameId=${gameId}">Yes</a>
    &nbsp;&nbsp;
    <a class="btn btn-danger" href="list">No</a>
</div>

<div class="w3-container w3-center">
    <img alt="fight.gif" src="${pageContext.request.contextPath}/resources/img/fight1.gif">
</div>

</body>
</html>