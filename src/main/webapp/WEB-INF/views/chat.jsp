<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/chat.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="//cdn.jsdelivr.net/sockjs/1/sockjs.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/chat.js"></script>
</head>
<body>
<div class='container'>
    <input type="hidden" id="player" value="${player.login}"/>
    <input type="hidden" id="avatar" value="${player.avatar}"/>
    <h1>Chat for players</h1>
    <div class='chatbox'>
        <!--User List-->
        <div class='chatbox__user-list'>
            <div id="user-list">
                <h1>Online</h1>
            </div>
            <button onclick="sendMessage();" id="send">Send</button>
            <%--<a href="/multigame/list"><button onclick="closeChat();" id="close">Close</button></a>--%>
            <a href="/list"><button onclick="closeChat();" id="close">Close</button></a>
        </div><!--End of User List-->

        <div class="chatbox__messages">
            <div class="chatbox__messages__user-message">
            </div>
        </div>

        <form>
            <input id="text" type="text" placeholder="Enter your message" value=""/>
        </form>

    </div></div>
</body>
</html>
