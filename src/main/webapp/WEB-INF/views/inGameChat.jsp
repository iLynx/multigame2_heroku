<!--Click-->
<div class="container">
    <div class="row">
        <div class="round hollow">
            <a href="#" id="addClass"><span class="glyphicon glyphicon-comment"></span></a>
        </div>
    </div>
</div>

<!--Popup-->
<div class="popup-box chat-popup" id="qnimate">
    <div class="popup-head">
        <div class="popup-head-left pull-left"></div>
        <div class="popup-head-right pull-right">
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-off"></i></button>
        </div>
    </div>

    <div class="popup-messages">

        <div class="direct-chat-messages">
        </div>

    </div>

    <div class="popup-messages-footer">
        <textarea id="status_message" placeholder="Type a message..." rows="10" cols="40" name="message"></textarea>
        <div class="btn-footer">
            <button class="btn btn-primary" onclick="sendMessage()">Send</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#addClass").click(function () {
            $('#qnimate').addClass('popup-box-on');
        });

        $("#removeClass").click(function () {
            $('#qnimate').removeClass('popup-box-on');
        });
    })

    $(document).ready(function () {
        connect();
        $('.popup-head-left').append('<img src="'+$('#avatar').val()+'" alt="iamgurdeeposahan">'+$('#player').val()+'')
    });
    $(window).bind("beforeunload", function() {
        return closeChat();
    });

    var stompClient = null;
    function connect() {
        var socket = new SockJS('/gameChat-websocket');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);

            stompClient.subscribe('/chat/getGameMessage/'+$('#idOfGame').val(), function (message) {
                var obj = JSON.parse(message.body);
                if (obj.player == $('#player').val()) {
                    $('.direct-chat-messages').append(' <div class="direct-chat-info clearfix"> ' +
                            '<span class="direct-chat-name">'+obj.player+'</span></div> ' +
                            '<img alt="iamgurdeeposahan" src="'+obj.avatar+'" class="direct-chat-img-right"> ' +
                            '<div class="direct-chat-text-right">'+obj.text+'</div>');
                } else {
                    $('.direct-chat-messages').append('<div class="direct-chat-info clearfix"> ' +
                            '<span class="direct-chat-name">'+obj.player+'</span> ' +
                            '</div><img alt="message user image" src="'+obj.avatar+'" class="direct-chat-img"> ' +
                            '<div class="direct-chat-text-left">'+obj.text+'</div>');
                }
            });
        });

    }
    function closeChat(){
        if (stompClient != null) {
            stompClient.disconnect();
        }
        console.log("Disconnected");
    }
    function sendMessage(){
        var msg = JSON.stringify(
                {
                    "player": $('#player').val(),
                    "text": $('#status_message').val(),
                    "avatar": $('#avatar').val()
                }
        );
        stompClient.send("/gameMsgSend/"+$('#idOfGame').val(), {}, msg);
        $('#status_message').val('');
    }
</script>