<%--
    @author Mikhail Shvets shvetsmihail@gmail.com
    @author Igor Khlaponin igor.boxmails@gmail.com
--%>

<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration page</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<spring:message code="label.registration.login.placeholder" var="loginPlaceholder"/>
<spring:message code="label.registration.password.placeholder" var="passwordPlaceholder"/>
<spring:message code="label.registration.email.placeholder" var="emailPlaceholder"/>
<spring:message code="label.registration.register" var="registr"/>
<spring:message code="label.registration.login" var="login"/>

<spring:message code="label.index.welcome.message" var="welcomeMessage"/>


<span id="lang-switch">
    <a href="#" onclick="changeLocale('en')">en</a> |
    <a href="#" onclick="changeLocale('ru')">ru</a>
</span>

<div id="welcome-container">
    <h2>${welcomeMessage}</h2>
</div>


<div class="container">
    <springForm:form class="form-horizontal" commandName="newPlayer" name="register" method="post" action="register">
        <h3><spring:message code="label.registration.message"/></h3>
        <br>
        <table>
            <tr>
                <td>
                    <div class="placeholder"><springForm:input path="login" cssClass="form-control"
                                                               placeholder="${loginPlaceholder}"/>
                    </div>
                </td>
                <td>
                    <div class="message"><springForm:errors path="login" cssClass="error"/></div>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="placeholder"><springForm:input path="password" cssClass="form-control"
                                                               placeholder="${passwordPlaceholder}"/>
                    </div>
                </td>
                <td>
                    <div class="message"><springForm:errors path="password" cssClass="error"/></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="placeholder"><springForm:input path="email" cssClass="form-control"
                                                               placeholder="${emailPlaceholder}"/>
                    </div>
                </td>
                <td>
                    <div class="message"><springForm:errors path="email" cssClass="error"/></div>
                </td>
            </tr>
        </table>
        <br>
        <input class="btn btn-success" type="submit" value=${registr}>
        <a class="btn btn-default" href="/">${login}</a>


    </springForm:form>
</div>

<div id="made-by" class="footer">
    made by Java-Dp-105 | SoftServe IT Academy
</div>

<script>
    $(document).ready(function () {
        changeButtonState();
    });

    function changeLocale(lang) {
        var url, currentUrl = window.location.href;
        if (window.location.search == '' || window.location.search == null) {
            url = currentUrl + '?lang=' + lang;
        } else {
            url = getUrlWithoutLocale() + 'lang=' + lang;
        }
        location.href = url;
    }

    function getUrlWithoutLocale() {
        var finalUrl, data, paramsArray, params = window.location.search.substr(1);
        data = '?';
        paramsArray = params.split('&');
        $.each(paramsArray, function (index, param) {
            if (param.indexOf('lang') == -1) {
                data += param + '&';
            }
        });
        finalUrl = window.location.href.substr(0, window.location.href.indexOf('?')) + data;
        return finalUrl;
    }

    function changeButtonState() {
        var locale = getLocaleFromCookie();
        if (locale === 'en') {
            $("#en").addClass('active');
            $("#ru").removeClass('active');
        } else if (locale === 'ru') {
            $("#ru").addClass('active');
            $("#en").removeClass('active');
        }
    }

    function getLocaleFromCookie() {
        var locale = 'en', cookie = document.cookie;

        cookie = cookie.split('; ');
        $.each(cookie, function (index, elem) {
            var pair = elem.split('=');
            if (pair[0] == 'org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE') {
                locale = pair[1].toString();
            }
        });
        return locale;
    }
</script>
</body>
</html>
