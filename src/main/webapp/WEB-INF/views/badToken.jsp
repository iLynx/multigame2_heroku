<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Bad Token</title>
    <link rel="stylesheet" href="https://bootswatch.com/paper/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" width="32" height="32"
          type="image/x-icon">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.1.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>

<spring:message code="label.index.welcome.message" var="welcomeMessage"/>
<spring:message code="label.registration.login" var="login"/>
<spring:message code="label.registration.register" var="registr"/>
<spring:message code="label.registration.bad.token" var="badToken"/>


<div id="welcome-container">
    <h2>${welcomeMessage}</h2>
</div>


<div class="container mg-center">
    <h3>${badToken}</h3>
    <br>
    <a class="btn btn-success" href="registration">${registr}</a>
</div>

<div id="made-by" class="footer">
    made by Java-Dp-105 | SoftServe IT Academy
</div>

</body>
</html>