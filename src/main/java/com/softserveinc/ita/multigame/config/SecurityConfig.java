package com.softserveinc.ita.multigame.config;


import com.softserveinc.ita.multigame.services.utils.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

/**
 * Security configuration
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private PasswordEncryptor passwordEncryptor;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .authoritiesByUsernameQuery("SELECT login as username, role as authority FROM players WHERE login = ?")
                .usersByUsernameQuery("SELECT login as username, password as password, enabled as enabled FROM players WHERE login = ?")
                .passwordEncoder(passwordEncryptor);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/register").permitAll()
                    .antMatchers("/registration").permitAll()
                    .antMatchers("/registrationConfirm").permitAll()
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/")
                    .loginProcessingUrl("/j_spring_security_check")
                    .permitAll()
                    .defaultSuccessUrl("/login")
                    .permitAll()
                    .failureUrl("/?error=true")
                    .usernameParameter("player_name")
                    .passwordParameter("player_pass")
                    .and()
                .logout()
                    .logoutUrl("/j_spring_security_logout")
                    .logoutSuccessUrl("/")
                    .permitAll()
                    .and()
                .csrf().disable();

    }

}
