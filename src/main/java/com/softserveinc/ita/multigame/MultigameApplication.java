package com.softserveinc.ita.multigame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Root class of Spring Boot application.
 * It contains main configuration of Spring Boot app and it's runner
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 * @since v2.0
 */

@SpringBootApplication
public class MultigameApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultigameApplication.class, args);
	}
}
