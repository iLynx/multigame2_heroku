package com.softserveinc.ita.multigame.model;

/**
 * @author Igor Khlaponi igor.boxmails@gmail.com
 */

public enum Gender {
    MALE,
    FEMALE
}
