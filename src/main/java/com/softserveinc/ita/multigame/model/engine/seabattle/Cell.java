package com.softserveinc.ita.multigame.model.engine.seabattle;
/**
 * Represents cell for Sea Battle field {@link SeaBattle}
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
public class Cell {
    private String value;
    private String address;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
