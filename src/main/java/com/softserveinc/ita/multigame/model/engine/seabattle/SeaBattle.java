package com.softserveinc.ita.multigame.model.engine.seabattle;
/**
 * Implementation of Game Engine for Sea Battle game
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.GenericGameEngine;

import java.util.List;
import java.util.function.Function;

public class SeaBattle extends GenericGameEngine{
    private char [][] firstPlayerField  = new char[10][10];
    private char [][] secondPlayerField = new char[10][10];
    public SeaBattle() {
        super();
        initFirstFieldDefault();
        initSecondFieldDefault();
    }
    private void initFirstFieldDefault(){
        firstPlayerField = new char[][]{        {'#','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'#','_','#', '_', '#','_','_', '_', '_', '_'},
                                                {'#','_','#', '_', '_','_','#', '_', '_', '_'},
                                                {'#','_','_', '_', '#','_','_', '_', '_', '_'},
                                                {'_','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','#', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','_', '_', '#','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'}
                                        };
    }
    private void initSecondFieldDefault(){
        secondPlayerField = new char[][]{       {'#','#','#', '#', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','#','#', '_', '#','#','#', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','#','_', '#', '#','_','#', '#', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'#','_','#', '_', '#','_','#', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'}
                                        };
    }
    private Function<String, Integer> getX = (str) -> str.charAt(0) - 97;
    private Function<String, Integer> getY = (str) ->   {if(str.length() == 2) return str.charAt(1)-48-1;
                                                            else return 9;};
    private boolean isActive() {
        boolean firstPlayerInGame=false;
        boolean secondPlayerInGame=false;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                char c1 = firstPlayerField [i][j];
                char c2 = secondPlayerField [i][j];
                if(c1 == '#')
                    secondPlayerInGame=true;
                if(c2 == '#')
                    firstPlayerInGame=true;
            }
        }
        if(!firstPlayerInGame || !secondPlayerInGame){
            if(!firstPlayerInGame)
                gameState = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
            else
                gameState = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }
        return firstPlayerInGame && secondPlayerInGame;
    }
    private boolean validatePlayerLogic(int x, int y, char[][]field){
        switch (field[x][y]){
            case 'X': return false;
            case '*': return false;
            default:  return true;
        }
        //TODO: neighbor cells validation
    }
    @Override
    protected boolean validateTurnLogic(String turn) {
        turn=turn.toLowerCase();
        int x = getX.apply(turn);
        int y = getY.apply(turn);

        if(gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN)
            return validatePlayerLogic(x, y, secondPlayerField);
        else if(gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN)
            return validatePlayerLogic(x,y, firstPlayerField);
        else return true;
    }

    @Override
    protected GameState changeGameState(Player player, String turn) {
        turn=turn.toLowerCase();
        int x = getX.apply(turn);
        int y = getY.apply(turn);

        if(player.equals(getFirstPlayer())){
            switch (secondPlayerField[x][y]){
                case '_':   if(!isActive())
                    return gameState;
                else{
                    secondPlayerField[x][y] = '*';
                    return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                }
                case '#':   secondPlayerField[x][y] = 'X';
                            if(!isActive())
                                return gameState;
                            else return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case 'X':   return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case '*':   return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                default:    return GameState.UNDEFINED;
            }
        }
        else {
            switch (firstPlayerField[x][y]){
                case '_':   firstPlayerField[x][y] = '*';
                    return GameState.WAIT_FOR_FIRST_PLAYER_TURN;
                case '#':   firstPlayerField[x][y] = 'X';
                            if(!isActive())
                                return gameState;
                            else return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                case 'X':   return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                case '*':   return GameState.WAIT_FOR_SECOND_PLAYER_TURN;
                default:    return GameState.UNDEFINED;
            }
        }
    }

    @Override
    public Object getBoard(Player player) {
        if(player.equals(getFirstPlayer()))
            return CellWrapper.wrapField(firstPlayerField);
        else return CellWrapper.wrapField(secondPlayerField);
    }

    @Override
    public boolean setField(Player player, List<Cell> field){
        char[][] settedField = CellWrapper.unwrapField(field);

        if(player.equals(getFirstPlayer())){
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    firstPlayerField[i][j]=settedField[i][j];
                }
            }
        }
        else{
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                   secondPlayerField[i][j]=settedField[i][j];
                }
            }
        }
        return true;
    }
}
