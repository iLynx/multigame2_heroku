package com.softserveinc.ita.multigame.model.engine.mill;

/**
 * Represents stones that a {@link MillPlayer} puts on the {@link Board}
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
class Stone {
    private String color;

    Stone(String color) {
        this.color = color;
    }

    String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stone stone = (Stone) o;
        return color != null ? color.equals(stone.color) : stone.color == null;
    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }
}

