package com.softserveinc.ita.multigame.model.engine.reversi;

/**
 * Class Color represents pawns colors for the {@link com.softserveinc.ita.multigame.model.engine.reversi.Reversi} game
 *
 * @author Igor Khlapon igor.boxmails@gmail.com
 * @since v1.0
 */

public class Color {
    public static final int WHITE = 1;
    public static final int BLACK = 2;
    public static final int EMPTY_CELL = 0;

    public static int getOpponentColor(int currentColor) {
        return currentColor == Color.BLACK ? Color.WHITE : Color.BLACK;
    }

    public static int getColor(int colorValue) {
        if (colorValue != Color.BLACK && colorValue != Color.WHITE
                && colorValue != Color.EMPTY_CELL) {
            throw new UnsupportedOperationException();
        }
        return colorValue == Color.BLACK
                ? Color.BLACK
                : (colorValue == Color.WHITE) ? Color.WHITE : Color.EMPTY_CELL;
    }

}
