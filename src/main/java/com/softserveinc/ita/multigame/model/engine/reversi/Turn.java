package com.softserveinc.ita.multigame.model.engine.reversi;

import java.util.Map;

/**
 * Wrapper for turn, which is made by user
 * for {@link com.softserveinc.ita.multigame.model.engine.reversi.Reversi} game
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 * @since v1.0
 */

public class Turn {

    private Integer key;
    private Integer value;

    public Turn(String turn) {
        for (Map.Entry<Integer, Integer> pair : ReversiUtil.turnParser(turn).entrySet()) {
            key = pair.getKey();
            value = pair.getValue();
        }
        if (key == null) {
            key = -1;
        }
        if (value == null) {
            value = -1;
        }
    }

    public int getKey() {
        return key;
    }

    public int getValue() {
        return value;
    }
}
