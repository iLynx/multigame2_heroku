package com.softserveinc.ita.multigame.model.engine.renju;

/**
 * Represents black, white stones and empty cells on {@link RenjuGame} board
 *
 * @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
 */
public enum BoardMarker {
    EMPTY(""), BLACK("B"), WHITE("W");

    private String boardMarker;

    BoardMarker(String s) {
        boardMarker = s;
    }

    public String getBoardMarker() {
        return boardMarker;
    }
}
