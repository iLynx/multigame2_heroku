package com.softserveinc.ita.multigame.model.confirmemail;

import com.softserveinc.ita.multigame.model.Player;
import org.springframework.context.ApplicationEvent;

/**
 * Extend {@link ApplicationEvent}. Set {@link Player} as source object
 * on which the event initially occurred
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */

public class OnRegistrationCompleteEvent extends ApplicationEvent {
    private Player player;
    private String appPath;

    public OnRegistrationCompleteEvent(Player player, String appPath) {
        super(player);
        this.player = player;
        this.appPath = appPath;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getAppPath() {
        return appPath;
    }

    public void setAppPath(String appPath) {
        this.appPath = appPath;
    }
}
