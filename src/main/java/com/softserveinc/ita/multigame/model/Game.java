package com.softserveinc.ita.multigame.model;

import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.model.engine.halma.HalmaGameEngine;
import com.softserveinc.ita.multigame.model.engine.mill.MillGameEngine;
import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;
import com.softserveinc.ita.multigame.model.engine.reversi.Reversi;
import com.softserveinc.ita.multigame.model.engine.rsp.RockScissorsPaper;
import com.softserveinc.ita.multigame.model.engine.seabattle.SeaBattle;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link com.softserveinc.ita.multigame.model.Game} is the wrapper for
 * {@link com.softserveinc.ita.multigame.model.engine.GameEngine} which contains actually gameEngine
 * for current game, game type, creation time (time when the game has been created), start time (time
 * when the game has been started - set when second player join to the game), and list of turns
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 * @since v1.0
 */

public class Game {

    private GameEngine gameEngine;
    private GameType gameType;
    private LocalDateTime creationTime;
    private LocalDateTime startTime;
    private List<TurnLog> turnList = new ArrayList<>();


    public Game(Player firstPlayer, GameType gameType) {
        this.gameType = gameType;
        setGameEngineByGameType(gameType);
        gameEngine.setFirstPlayer(firstPlayer);
        this.creationTime = LocalDateTime.now();
    }

    public boolean joinToGame(Player player) {
        if (gameEngine.setSecondPlayer(player)) {
            startTime = LocalDateTime.now();
            return true;
        }
        return false;
    }

    public GameEngine getGameEngine() {
        return gameEngine;
    }

    public void setGameEngine(GameEngine gameEngine) {
        this.gameEngine = gameEngine;
    }

    public GameType getGameType() {
        return gameType;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public List<TurnLog> getTurnList() {
        return turnList;
    }

    public void setTurnList(List<TurnLog> turnList) {
        this.turnList = turnList;
    }

    public boolean makeTurn(Player player, String turn) {

        if (gameEngine.makeTurn(player, turn)) {
            turnList.add(new TurnLog(this.getGameEngine().getId(), player.getLogin(), turn));
            return true;
        } else {
            return false;
        }
    }

    private void setGameEngineByGameType(GameType gameType) {

        switch (gameType) {
            case REVERSI: {
                gameEngine = new Reversi();
                break;
            }

            case HALMA: {
                gameEngine = new HalmaGameEngine();
                break;
            }

            case RENJU: {
                gameEngine = new RenjuGame();
                break;
            }

            case MILL: {
                gameEngine = new MillGameEngine();
                break;
            }

            case RSP: {
                gameEngine = new RockScissorsPaper();
                break;
            }

            case SEABATTLE: {
                gameEngine = new SeaBattle();
                break;
            }

            default: {
                throw new UnsupportedOperationException("Unsupported game type");
            }
        }
    }

    @Override
    public String toString() {
        return gameEngine.toString();
    }
}
