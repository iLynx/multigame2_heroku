package com.softserveinc.ita.multigame.model.engine.seabattle;

import java.util.ArrayList;
import java.util.List;
/**
 * Util class to wrap/unwrap field for Sea Battle game {@link SeaBattle}
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
public class CellWrapper {
    public static List<Cell> wrapField(char[][] field){
        List<Cell> newField = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Cell cell = new Cell();
                cell.setValue(String.valueOf(field[i][j]));
                char l=' ';
                switch (i){
                    case 0: l='a';break;
                    case 1: l='b';break;
                    case 2: l='c';break;
                    case 3: l='d';break;
                    case 4: l='e';break;
                    case 5: l='f';break;
                    case 6: l='g';break;
                    case 7: l='h';break;
                    case 8: l='i';break;
                    case 9: l='j';break;
                }
                cell.setAddress(l+""+(j+1));
                newField.add(cell);
            }
        }
        return newField;
    }
    public static char[][] unwrapField(List<Cell> field){
        char[][] newField = new char[10][10];
        int k=0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                newField[i][j] = field.get(k).getValue().charAt(0);
                k++;
            }
        }


        return newField;
    }
}
