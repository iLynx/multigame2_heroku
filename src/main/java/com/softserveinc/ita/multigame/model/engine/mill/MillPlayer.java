package com.softserveinc.ita.multigame.model.engine.mill;

import java.util.ArrayList;
import java.util.List;

/**
 * Player in MillGame. MillPlayer has 9 stones of one color in Hand, when he was created.
 * When getNewStone method is called, MillPlayer moves the stone from stonesInHand, to stonesInGame
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
class MillPlayer {
    private String color;
    private List<Stone> stonesInHand = new ArrayList<>();
    private List<Stone> stonesInGame = new ArrayList<>();

    MillPlayer(String color) {
        this.color = color;
        initStones();
    }

    String getColor() {
        return color;
    }

    Stone getNewStone() {
        Stone stone = stonesInHand.remove(0);
        stonesInGame.add(stone);
        return stone;
    }

    void dropStone(Stone stone) {
        stonesInGame.remove(stone);
    }

    int getCountOfStones() {
        return stonesInGame.size() + stonesInHand.size();
    }

    int getCountOfStonesInHand() {
        return stonesInHand.size();
    }

    private void initStones() {
        for (int i = 0; i < 9; i++) {
            stonesInHand.add(new Stone(color));
        }
    }
}
