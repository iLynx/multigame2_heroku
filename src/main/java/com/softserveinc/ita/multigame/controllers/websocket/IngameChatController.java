package com.softserveinc.ita.multigame.controllers.websocket;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Controller for inGame chat
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
@Controller
public class IngameChatController {

    @MessageMapping("/gameMsgSend/{gameId}")
    @SendTo("/chat/getGameMessage/{gameId}")
    public ChatMessage chatting(ChatMessage message, @DestinationVariable Long gameId) throws Exception {
        return message;
    }
}
