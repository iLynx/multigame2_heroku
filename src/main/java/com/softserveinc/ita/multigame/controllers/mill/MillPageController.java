package com.softserveinc.ita.multigame.controllers.mill;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

/**
 * Represents millGame.jsp
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
@Controller
@RequestMapping("/mill")
public class MillPageController {
    private Logger logger = Logger.getLogger(MillPageController.class);

    private GameListService gameListService;
    @Autowired
    public MillPageController(GameListService gameListService) {
        this.gameListService = gameListService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@SessionAttribute Player player,
                          @RequestParam("gameId") Long gameId,
                          ModelMap model) {

        Game game = gameListService.getGame(gameId);
        GameEngine gameEngine = game.getGameEngine();

        logger.debug(player.getLogin() + " connect to created game");

        Player opponent;
        if (player.equals(gameEngine.getFirstPlayer())) {
            opponent = gameEngine.getSecondPlayer();
        } else {
            opponent = gameEngine.getFirstPlayer();
        }

        model.addAttribute("player", player);
        model.addAttribute("opponent", opponent);
        model.addAttribute("gameId", gameId);
        return "mill/millGame";
    }
}
