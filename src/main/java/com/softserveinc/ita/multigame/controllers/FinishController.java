package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.Locale;
/**
 * Controller for finish page.
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
@Controller
public class FinishController {

    GameHistoryService gameHistoryService;

    private MessageSource messageSource;
    @Autowired
    public FinishController(GameHistoryService gameHistoryService, MessageSource messageSource) {
        this.gameHistoryService = gameHistoryService;
        this.messageSource = messageSource;
    }

    @RequestMapping("/finish")
    public String finish(@SessionAttribute("player") Player player, ModelMap model,
                         @RequestParam("gameId") Long gameId, Locale locale) {
        Player winner = gameHistoryService.getWinner(gameId);
        Player loser = gameHistoryService.getLoser(gameId);

        if (player.equals(winner)) {
            model.addAttribute("message", messageSource.getMessage("label.finish.win", new Object[0], locale));
            model.addAttribute("image", "resources/img/win.png");
        } else if (player.equals(loser)) {
            model.addAttribute("message", messageSource.getMessage("label.finish.lose", new Object[0], locale));
            model.addAttribute("image", "resources/img/lose.png");
        } else {
            model.addAttribute("message", messageSource.getMessage("label.finish.draw", new Object[0], locale));
            model.addAttribute("image", "resources/img/draw.png");
        }
        return "finish";
    }
}
