package com.softserveinc.ita.multigame.controllers.reversi;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Represents reversiGamePage.jsp
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

@Controller
@RequestMapping("/reversi")
public class ReversiPageController {

    private Logger logger = Logger.getLogger(ReversiPageController.class);

    GameListService gameListService;

    @Autowired
    public ReversiPageController(GameListService gameListService) {
        this.gameListService = gameListService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getCurrentReversiGamePage(@SessionAttribute Player player,
                                            @RequestParam("gameId") Long gameId, ModelMap model) {

        Game currentGame = gameListService.getGame(gameId);

        Player opponent = (currentGame.getGameEngine().getFirstPlayer().equals(player))
                ? currentGame.getGameEngine().getSecondPlayer()
                : currentGame.getGameEngine().getFirstPlayer();
        model.addAttribute("opponent", opponent);
        model.addAttribute("matrix", currentGame.getGameEngine().getBoard());
        model.addAttribute("gameId", gameId);
        model.addAttribute("resultCode", currentGame.getGameEngine().getResultCode());
        String color = currentGame.getGameEngine().getFirstPlayer().equals(player)
                ? "white"
                : "black";
        model.addAttribute("pawnColor", color);
        logger.info("redirect to the Reversi game page");

        return "reversi/reversiGamePage";
    }

    @ResponseBody
    @RequestMapping(value = "/getResultCode", method = RequestMethod.GET)
    public String getGameResultCode(@RequestParam("gameId") Long gameId) {
        logger.debug("return result code for current Reversi game #" + gameId);
        return gameListService.getGame(gameId).getGameEngine().getResultCode().toString();
    }

}
