package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.services.EmailService;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for all actions that admin now can do.
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    private Logger logger = Logger.getLogger(AdminController.class);

    @Autowired
    PlayerService playerService;
    @Autowired
    EmailService emailService;

    @RequestMapping(path = "/delete/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable("id") Long id){
        playerService.delete(id);
        return "redirect:/players";
    }

    @RequestMapping(path = "/enable/{id}", method = RequestMethod.POST)
    public String setEnable(@PathVariable("id") Long id, @RequestParam("enabled") Boolean enabled){
        playerService.setEnabled(id, enabled);
        return "redirect:/players";
    }

    @RequestMapping(path = "/setAdmin/{id}", method = RequestMethod.POST)
    public String setAdmin(@PathVariable("id") Long id, @RequestParam("isAdmin") Boolean isAdmin){
        playerService.setAdmin(id, isAdmin);
        return "redirect:/players";
    }

    @RequestMapping(path = "/sendEmail", method = RequestMethod.POST)
    public String sendEmailToPlayer(@RequestParam("id") Long id,
                                    @RequestParam(value = "subject", required = false) String subject,
                                    @RequestParam("text") String text){
        emailService.sendToPlayer(playerService.get(id), subject, text);
        return "redirect:/players";
    }

    @RequestMapping(path = "/sendEmailToAll", method = RequestMethod.POST)
    public String sendEmailToAll(@RequestParam(value = "subject", required = false) String subject,
                               @RequestParam("text") String text){
        emailService.sendToAll(subject, text);
        return "redirect:/players";
    }


}
