package com.softserveinc.ita.multigame.controllers.rsp;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for Rock-Paper-Scissors-Lizard-Spock page
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

@Controller
@RequestMapping("/rsp")
public class RspPageController {
    private Logger logger = Logger.getLogger(RspPageController.class);

    GameListService gameListService;

    @Autowired
    public RspPageController(GameListService gameListService) {
        this.gameListService = gameListService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@SessionAttribute Player player, @RequestParam("gameId") Long gameId, ModelMap model) {

        GameEngine gameEngine = gameListService.getGame(gameId).getGameEngine();
        Player firstPlayer = gameEngine.getFirstPlayer();
        Player secondPlayer = gameEngine.getSecondPlayer();
        gameEngine.setFirstPlayer(firstPlayer);
        gameEngine.setSecondPlayer(secondPlayer);

        secondPlayer = player.equals(gameEngine.getFirstPlayer()) ? gameEngine.getSecondPlayer()
                : gameEngine.getFirstPlayer();

        model.addAttribute("firstPlayer", firstPlayer == null ? "" : firstPlayer);
        model.addAttribute("secondPlayer", secondPlayer == null ? "" : secondPlayer);
        model.addAttribute("gameId", gameId);
        model.addAttribute("currentPlayer", player);
        logger.debug("Current player is  " + player + "  " + "1st player is "
                + firstPlayer + " 2nd player is  " + secondPlayer);

        return "rsp/rspGame";
    }

    @ResponseBody
    @RequestMapping(value = "/refreshTurn", method = RequestMethod.GET)
    public TipDTO refreshPlayerTurn(@SessionAttribute Player player, @RequestParam("gameId") Long gameId) {
        logger.debug("refreshPlayerTurn");
        return gameListService.getTips(gameId, player);
    }
}
