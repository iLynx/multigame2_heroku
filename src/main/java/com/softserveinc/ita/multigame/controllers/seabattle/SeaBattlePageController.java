package com.softserveinc.ita.multigame.controllers.seabattle;

import com.softserveinc.ita.multigame.controllers.GameListController;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * Controller for Sea Battle game
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
@Controller
@RequestMapping("/seabattle")
public class SeaBattlePageController {
    private Logger logger = Logger.getLogger(GameListController.class);
    @Autowired
    private GameListService gameListService;

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@RequestParam("gameId") Long gameId, @SessionAttribute Player player, Model model) {
        Game game = gameListService.getGame(gameId);
        Player player2;
        if (player.equals(game.getGameEngine().getFirstPlayer())) {
            player2 = game.getGameEngine().getSecondPlayer();
        } else {
            player2 = game.getGameEngine().getFirstPlayer();
        }
        model.addAttribute("anotherPlayer", player2);
        return "seaBattle/gameInfo";
    }

    @ResponseBody
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public List<Cell> showBoard1(@SessionAttribute("player") Player player, @RequestParam("id") Long gameId) throws IOException {
        Game game = gameListService.getGame(gameId);
        List<Cell> board;
        if (game == null) {
            board = new ArrayList<>();
            Cell cell = new Cell();
            cell.setAddress("TheEnd");
            cell.setValue("TheEnd");
            board.add(cell);
            return board;
        } else {
            board = (List<Cell>) game.getGameEngine().getBoard(player);
            return board;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/show2", method = RequestMethod.GET)
    public List<Cell> showBoard2(@SessionAttribute("player") Player player, @RequestParam("id") Long gameId) throws IOException {
        Game game = gameListService.getGame(gameId);
        Player anotherPlayer;
        List<Cell> board;
        if (game == null) {
            board = new ArrayList<>();
            Cell cell = new Cell();
            cell.setAddress("TheEnd");
            cell.setValue("TheEnd");
            board.add(cell);
        } else {
            if (player.equals(game.getGameEngine().getFirstPlayer())) {
                anotherPlayer = game.getGameEngine().getSecondPlayer();
            } else {
                anotherPlayer = game.getGameEngine().getFirstPlayer();
            }
            board = (List<Cell>) gameListService.getGame(gameId).getGameEngine().getBoard(anotherPlayer);
        }
        return board;
    }
}
