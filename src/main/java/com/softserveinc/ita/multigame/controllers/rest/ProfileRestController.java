package com.softserveinc.ita.multigame.controllers.rest;


import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

/**
 * RestController for all CRUD methods with players
 *  @author Andrew Volyk andredewoll@gmail.com avolyk1tc
*
*        PUT        /api/v.0.1/player        - create player
*        POST       /api/v.0.1/player/id        - update player
*        DELETE     /api/v.0.1/player/id        - delete player
*        GET        /api/v.0.1/player/id        - read player
*        GET        /api/v.0.1/players/        - read player list
*/

@RestController
@RequestMapping("/api/v.0.1")
public class ProfileRestController {

    private Logger logger = Logger.getLogger(ProfileRestController.class);
    private PlayerService playerService;

    @Autowired
    public ProfileRestController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @PutMapping(value = "/player", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createPlayer(@RequestBody  @Valid Player player, BindingResult bindingResult,
                                             UriComponentsBuilder builder, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (playerService.getByLogin(player.getLogin()) == null) {

            player.setRegistrationTime(LocalDateTime.now());
            String appPath = request.getRequestURL().substring(0, request.getRequestURL().indexOf("/api/v.0.1/player"));
            player.setAvatar(appPath + "/resources/avatars/default.png");
            playerService.saveOrUpdate(player);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(builder.path("/{id}").
                    buildAndExpand(player.getId()).toUri());
            logger.info("HttpStatus: CREATED");
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @GetMapping(value = "/players", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Player>> readPlayerList() {
        List<Player> playerList = playerService.getAll();

        if (playerList.isEmpty()) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT) ;
        }
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(playerList, HttpStatus.OK);
    }

    @GetMapping(value = "/player/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Player> readPlayer (@PathVariable("id") Long playerId) {
        Player player = playerService.get(playerId);

        if (player != null) {
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(player, HttpStatus.OK);
        }
        logger.error("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "/player/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updatePlayer(@PathVariable("id") Long playerId, @RequestBody  @Valid Player player,
                                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.error(" BAD REQUEST");
            return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
        }
        player.setId(playerId);

        if (playerService.saveOrUpdate(player) != null) {
            logger.info("HTTP Status: OK" + playerId );
            return new ResponseEntity<>(HttpStatus.OK);
        }
        logger.error("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/player/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deletePlayer(@PathVariable("id") Long playerId) {
        playerService.delete(playerId);
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
