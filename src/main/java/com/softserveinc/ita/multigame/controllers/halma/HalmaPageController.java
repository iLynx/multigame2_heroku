package com.softserveinc.ita.multigame.controllers.halma;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpServlet;
import java.util.Locale;
/**
 * Controller for Halma page
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
@Controller
@RequestMapping("/halma")
public class HalmaPageController extends HttpServlet {

    private Logger logger = Logger.getLogger(HalmaPageController.class);

    private GameListService gameListService;
    private MessageSource messageSource;

    @Autowired
    public HalmaPageController(GameListService gameListService, MessageSource source) {
        this.gameListService = gameListService;
        this.messageSource = source;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage(@RequestParam("gameId") Long gameId, Locale locale,
                          ModelMap model, @SessionAttribute Player player){
        Game game = gameListService.getGame(gameId);
        Player opponent = null;
        String color = null;

        if (game.getGameEngine().getFirstPlayer().equals(player)) {
            opponent = game.getGameEngine().getSecondPlayer();
            color = messageSource.getMessage("label.halma.greenColor", new Object[0], locale);
        } else if (game.getGameEngine().getSecondPlayer().equals(player)) {
            opponent = game.getGameEngine().getFirstPlayer();
            color = messageSource.getMessage("label.halma.blueColor", new Object[0], locale);
        }
        logger.debug(color);

        model.addAttribute("opponent", opponent);
        model.addAttribute("color", color);
        model.addAttribute("gameId", gameId);
        model.addAttribute("currentPlayer", player);

        return "halma/halmaPage";
    }
}
