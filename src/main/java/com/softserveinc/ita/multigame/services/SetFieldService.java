package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.engine.seabattle.Cell;
import com.softserveinc.ita.multigame.model.engine.seabattle.CellWrapper;
import com.softserveinc.ita.multigame.model.engine.seabattle.SeaBattle;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * Service to set and validate field for {@link SeaBattle}
 *
 * @author Daniel Sokyrynskyi sokirinskiy@gmail.com
 */
@Service
public class SetFieldService {
    public List<Cell> getEmptyField(){
        char [][] field = new char[][]{         {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'},
                                                {'_','_','_', '_', '_','_','_', '_', '_', '_'}
                                        };
        return CellWrapper.wrapField(field);
    }
    public boolean validateField(List<Cell> field){
        int count = 0;
        for (Cell cell: field){
            if(cell.getValue().equals("#"))
                count++;
        }
        return count == 20;
    }
}
