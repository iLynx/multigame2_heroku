package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.TurnLog;

import java.util.List;

/**
 * Service for CRUD operations with GameHistory entity
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */

public interface GameHistoryService {

    void saveGameHistoryForGame(Game game);

    List<GameHistory> getGameHistoriesForPlayer(Player player);

    GameHistory getGameHistoryById(Long id);

    void removeGameHistoriesForPlayer(Player player);

    Player getWinner(Long gameId);

    Player getLoser(Long gameId);

    List<TurnLog> getTurnsByGameId(Long gameId);
}