package com.softserveinc.ita.multigame.services.utils;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * PasswordEncryptor contains methods for encryption and decryption of user's passwords
 * Provide strong encryption by Jasypt library (SHA-256 encryption algorithm, 16 bytes salt size)
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

@Component
public class PasswordEncryptor implements PasswordEncoder{

    @Override
    public String encode(CharSequence password) {
        StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
        return passwordEncryptorStrong.encryptPassword(password.toString());
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encryptedPassword) {
        StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
        return passwordEncryptorStrong.checkPassword(rawPassword.toString(), encryptedPassword);
    }

}
