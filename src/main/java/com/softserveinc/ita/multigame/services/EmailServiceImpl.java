package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Allows to send emails to players
 * Implementation of {@link EmailService}
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
@Service
public class EmailServiceImpl implements EmailService {
    private Logger logger = Logger.getLogger(EmailServiceImpl.class);

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    PlayerService playerService;

    @Override
    public void sendToPlayer(Player player, String subject, String text) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(player.getEmail());
        email.setSubject(subject);
        email.setText(text);
        mailSender.send(email);
        logger.debug("email was sent to player #" + player.getId());

    }

    @Override
    public void sendToAll(String subject, String text) {
        SimpleMailMessage email = new SimpleMailMessage(); //TODO remove "new", inject with spring
        List<String> allEmail = new ArrayList<>();
        playerService.getAll()
                .forEach(p -> allEmail.add(p.getEmail()));

        email.setTo(allEmail.toArray(new String[0]));
        email.setSubject(subject);
        email.setText(text);
        mailSender.send(email);
    }
}
