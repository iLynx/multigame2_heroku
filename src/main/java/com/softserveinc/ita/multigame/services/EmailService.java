package com.softserveinc.ita.multigame.services;

import com.softserveinc.ita.multigame.model.Player;

/**
 * Allows to send emails to players
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
public interface EmailService {

    void sendToPlayer(Player player, String subject, String text);
    void sendToAll(String subject, String text);
}
