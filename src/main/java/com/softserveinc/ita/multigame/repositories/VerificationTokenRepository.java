package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.confirmemail.VerificationToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Repository for CRUD operation with {@link VerificationToken} and save it to DB
 * Extends Spring Data CrudRepository
 * Contain custom method findByToken
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
public interface VerificationTokenRepository extends CrudRepository<VerificationToken, Long> {
    @Query("select vt from VerificationToken vt where vt.token = :token")
    VerificationToken findByToken(@Param("token") String token);

}
