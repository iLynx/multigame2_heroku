package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.TurnLog;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Repository for saving history of turns to MongoDB
 *
 * @author Vladimir Serdiuk vvserdiuk@gmail.com
 */
public interface TurnLogRepository extends MongoRepository<TurnLog, String> {

    List<TurnLog> findByGameId(Long gameId);
}
