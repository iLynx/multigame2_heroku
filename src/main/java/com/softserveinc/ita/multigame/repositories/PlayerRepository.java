package com.softserveinc.ita.multigame.repositories;

import com.softserveinc.ita.multigame.model.Player;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Repository for CRUD operation with {@link Player} and save it to DB
 * Extends Spring Data CrudRepository
 * Contain custom methods: findByLogin and findAll
 *
 * @author Mikhail Shvets shvetsmihail@gmail.com
 */
public interface PlayerRepository extends CrudRepository<Player, Long> {
    @Query("select p from Player p where p.login = :login")
    Player findByLogin(@Param("login") String login);

    @Query("select p from Player p")
    List<Player> findAll();
}