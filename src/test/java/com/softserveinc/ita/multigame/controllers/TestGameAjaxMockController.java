package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.TipDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Unit Testing GameAjax controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

public class TestGameAjaxMockController {

    private Long gameId = 9L;
    Player firstPlayer = new Player(1L, "Alex", "111");
    Game game = new Game(firstPlayer, GameType.REVERSI);
    String turn = "p";
    private Object board = game.getGameEngine().getBoard();
    TipDTO dto = new TipDTO(firstPlayer,"message");

    private GameListService gameListService;
    private GameAjaxController controller;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        controller = new GameAjaxController(gameListService);
    }

    @Test
    public void testingGettingBoard() {
        //Setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        //Execute
        Object board1 = controller.getBoard(gameId);
        //Verify
        assertNotEquals(board1, board );
    }

    @Test
    public void testGettingTipsTurnReturnsProperDTO() {
        //setup
        Mockito.when(gameListService.getTips(gameId, firstPlayer)).thenReturn(dto);
        //Execute
        TipDTO dto1 = controller.getTips(gameId, firstPlayer);
        //Verify
        assertEquals(dto1, dto);
    }


}
