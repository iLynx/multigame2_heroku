package com.softserveinc.ita.multigame.controllers.rest;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameHistoryService;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for {@link GameHistoryRestController}
 *
 * @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
 */
@Category(IntegrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testmg")
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class GameHistoryRestControllerTest {

	final String BASE_URL = "/api/v.0.1";
	GameType gameType = GameType.RENJU;

	@Autowired
	private GameHistoryRestController controllerToTest;

	@Autowired
	PlayerService playerService;

	@Autowired
	GameListService gameListService;

	@Autowired
	GameHistoryService gameHistoryService;

	private MockMvc mockMvc;
	private Player p1;
	private Player p2;

	@Before
	public void setUp() {
		Player player1 = new Player("111", "111");
		player1.setEmail("aaa@gmail.com");
		playerService.saveOrUpdate(player1);
		Player player2 = new Player("222", "222");
		player2.setEmail("bbb@gmail.com");
		playerService.saveOrUpdate(player2);

		p1 = playerService.getByLogin("111");
		p2 = playerService.getByLogin("222");

		mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
		controllerToTest = mock(GameHistoryRestController.class);
	}

	@Test
	public void testGetGameHistoriesForPlayer() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		gameHistoryService.saveGameHistoryForGame(newGame);

		mockMvc.perform(get(BASE_URL + "/gameHistories/" + p1.getId())
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}

	@Test
	public void testGetGameHistory() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		gameHistoryService.saveGameHistoryForGame(newGame);

		mockMvc.perform(get(BASE_URL + "/gameHistory/" + newGame.getGameEngine().getId())
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(jsonPath("$.id", is(newGame.getGameEngine().getId().intValue())));
	}

	@Test
	public void testGetTurnLog() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		newGame.joinToGame(p2);
		newGame.makeTurn(p1, "1,1");
		newGame.makeTurn(p2, "11,7");
		gameHistoryService.saveGameHistoryForGame(newGame);

		mockMvc.perform(get(BASE_URL + "/turnLog/" + newGame.getGameEngine().getId())
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}

	@Test
	public void testGetGameWinner() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		newGame.joinToGame(p2);
		newGame.makeTurn(p1, "1,1");
		newGame.makeTurn(p2, "11,7");
		newGame.makeTurn(p1, "2,2");
		newGame.makeTurn(p2, "11,12");
		newGame.makeTurn(p1, "3,3");
		newGame.makeTurn(p2, "10,12");
		newGame.makeTurn(p1, "4,4");
		newGame.makeTurn(p2, "12,12");
		newGame.makeTurn(p1, "5,5");
		gameHistoryService.saveGameHistoryForGame(newGame);

		mockMvc.perform(get(BASE_URL + "/gameWinner/" + newGame.getGameEngine().getId())
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(jsonPath("$.id", is(p1.getId().intValue())));
	}

	@Test
	public void testGetGameLoser() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		newGame.joinToGame(p2);
		newGame.makeTurn(p1, "1,1");
		newGame.makeTurn(p2, "11,7");
		newGame.makeTurn(p1, "2,2");
		newGame.makeTurn(p2, "11,12");
		newGame.makeTurn(p1, "3,3");
		newGame.makeTurn(p2, "10,12");
		newGame.makeTurn(p1, "4,4");
		newGame.makeTurn(p2, "12,12");
		newGame.makeTurn(p1, "5,5");
		gameHistoryService.saveGameHistoryForGame(newGame);

		mockMvc.perform(get(BASE_URL + "/gameLoser/" + newGame.getGameEngine().getId())
				.contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(jsonPath("$.id", is(p2.getId().intValue())));
	}

}
