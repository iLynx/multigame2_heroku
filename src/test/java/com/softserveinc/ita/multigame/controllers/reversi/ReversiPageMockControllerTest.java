package com.softserveinc.ita.multigame.controllers.reversi;



import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing Reversi game controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

public class ReversiPageMockControllerTest {
    Player currentPlayer = new Player(4L, "Beer", "111");
    Player opponent = new Player(6L, "Vodka", "111");
    Long gameId = 8L;
    Game game = new Game(opponent, GameType.REVERSI);
    String resultCode = "OK";

    private GameListService gameListService;
    private ReversiPageController controller;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        controller = new ReversiPageController(gameListService);
    }

    @Test
    public void testingThatGetPageReversiRedirectsToTheViewAndPutsAttributesTrue() {
        //Setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        opponent = (game.getGameEngine().getFirstPlayer().equals(currentPlayer))
                ? game.getGameEngine().getSecondPlayer()
                : game.getGameEngine().getFirstPlayer();
        ModelMap model = new ModelMap();
        //Execute
        String viewName = controller.getCurrentReversiGamePage(currentPlayer, gameId, model);
        //Verify
        assertEquals(viewName, "reversi/reversiGamePage");
        assertEquals(model.get("gameId"),gameId);
    }

    @Test
    public void testingGettingGameResultCode() {
        //Setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        String code = game.getGameEngine().getResultCode().toString();
        //Execute
        String  code1 = controller.getGameResultCode(gameId);
        //Verify
        assertEquals(code1, code);
    }
}
