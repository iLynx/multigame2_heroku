package com.softserveinc.ita.multigame.controllers.renju;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing Renju game controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */
public class RenjuPageMockTestContoller {
    Player currentPlayer = new Player(1L, "Beer", "111");
    Player opponent = new Player(2L, "Vodka", "111");
    Long gameId = 2L;
    Game game = new Game(currentPlayer, GameType.RENJU);

    private GameListService gameListService;
    private RenjuPageController controller;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        controller = new RenjuPageController(gameListService);
    }
    @Test
    public void testingThatGetPageRenjuRedirectsToTheViewAndPutsAttributesTrue() {
        //Setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        GameEngine gameEngine = game.getGameEngine();
        currentPlayer = gameEngine.getFirstPlayer();
        opponent = gameEngine.getSecondPlayer();
        ModelMap model = new ModelMap();
        //Execute
        String viewName = controller.getPage(currentPlayer, gameId, model);
        //Verify
        assertEquals(viewName, "renju/renjuGame");
        assertEquals(model.get("currentPlayer"), currentPlayer);
        assertEquals(model.get("gameId"),gameId);


    }

}
