package com.softserveinc.ita.multigame.controllers.halma;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameEngine;
import com.softserveinc.ita.multigame.services.GameListService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

/**
 * Unit Testing Halma game controller
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

public class HalmaPageMockTestController {
    Player currentPlayer = new Player(1L, "Gin", "111");
    Player opponent = new Player(2L, "Tonic", "111");
    Long gameId = 1L;
    Game game = new Game(currentPlayer, GameType.HALMA);
    String color = "green";
    Locale locale;

    private GameListService gameListService;
    private MessageSource messageSource;
    private HalmaPageController halmaPageController;

    @Before
    public void init() {
        gameListService = Mockito.mock(GameListService.class);
        messageSource = Mockito.mock(MessageSource.class);
        halmaPageController = new HalmaPageController(gameListService, messageSource);
    }

    @Test
    public void testingThatGetPageHalmaRedirectsToTheViewAndPutsAttributesTrue() {
        //setup
        Mockito.when(gameListService.getGame(gameId)).thenReturn(game);
        GameEngine gameEngine = game.getGameEngine();
        currentPlayer = gameEngine.getFirstPlayer();
        opponent = gameEngine.getSecondPlayer();
        Mockito.when(messageSource.getMessage("label.halma.greenColor", new Object[0], locale)).thenReturn(color);
        ModelMap model = new ModelMap();

        //Execute
        String viewName = halmaPageController.getPage(gameId, locale, model, currentPlayer);

        //Verify
        assertEquals(viewName, "halma/halmaPage");
        assertEquals(model.get("currentPlayer"), currentPlayer);
        assertEquals(model.get("gameId"),gameId);

    }

}
