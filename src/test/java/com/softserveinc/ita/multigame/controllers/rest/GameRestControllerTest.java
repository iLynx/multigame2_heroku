package com.softserveinc.ita.multigame.controllers.rest;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.softserveinc.ita.multigame.IntegrationTest;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameType;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.GameListService;
import com.softserveinc.ita.multigame.services.PlayerService;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Tests for {@link GameRestController}
 *
 * @author Artem Dvornichenko artem.dvornichenko@gmail.com advorntc
 */
@Category(IntegrationTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testmg")
@DirtiesContext
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class GameRestControllerTest {
	
	final String BASE_URL = "/api/v.0.1";
	GameType gameType = GameType.RENJU;

    @Autowired
    private GameRestController controllerToTest;
    
    @Autowired
    PlayerService playerService;
    
    @Autowired
    GameListService gameListService;

    private MockMvc mockMvc;
    private Player p1;
    private Player p2;

	@Before
	public void setUp() {
		Player player1 = new Player("111", "111");
		player1.setEmail("aaa@gmail.com");
		playerService.saveOrUpdate(player1);
		Player player2 = new Player("222", "222");
		player2.setEmail("bbb@gmail.com");
		playerService.saveOrUpdate(player2);
		
		p1 = playerService.getByLogin("111");
		p2 = playerService.getByLogin("222");

		mockMvc = MockMvcBuilders.standaloneSetup(controllerToTest).build();
		controllerToTest = mock(GameRestController.class);
	}

	@Test
	public void testCreateGame() throws Exception {

		mockMvc.perform(put(BASE_URL + "/game").sessionAttr("player", p1).contentType(MediaType.APPLICATION_JSON)
				.content(gameType.getGameName().toUpperCase()))
				.andExpect(status().isCreated());
	}
	
	@Test
	public void testJoinGame() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);

		mockMvc.perform(post(BASE_URL + "/join/" + newGame.getGameEngine().getId()).sessionAttr("player", p2).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void testGetCreatedGames() throws Exception {

		gameListService.createGame(p1, gameType);

		mockMvc.perform(get(BASE_URL + "/createdGames/" + p1.getId()).contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}
	
	@Test
	public void testGetWaitingGames() throws Exception {

		gameListService.createGame(p1, gameType);

		mockMvc.perform(get(BASE_URL + "/waitingGames/" + p2.getId()).contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}
	
	@Test
	public void testGetPlayingGames() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		newGame.joinToGame(p2);

		mockMvc.perform(get(BASE_URL + "/playingGames/" + p1.getId()).contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}
	
	@Test
	public void testGetGame() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		
		mockMvc.perform(get(BASE_URL + "/game/" + newGame.getGameEngine().getId()).contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
	}
	
	@Test
	public void testDeleteGame() throws Exception {

		Game newGame = gameListService.createAndReturnGame(p1, gameType);
		
		mockMvc.perform(delete(BASE_URL + "/game/" + newGame.getGameEngine().getId()).contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andExpect(status().isOk());
	}

}
