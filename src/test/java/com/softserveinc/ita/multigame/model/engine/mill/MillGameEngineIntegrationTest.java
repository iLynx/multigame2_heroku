package com.softserveinc.ita.multigame.model.engine.mill;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public class MillGameEngineIntegrationTest {
    private MillGameEngine mill = new MillGameEngine();
    private Player firstPlayer = new Player();
    private Player secondPlayer = new Player();

    @Before
    public void setUp() {
        firstPlayer.setId(1L);
        firstPlayer.setLogin("111");
        secondPlayer.setId(2L);
        secondPlayer.setLogin("222");
        mill.setFirstPlayer(firstPlayer);
        mill.setSecondPlayer(secondPlayer);
    }

    @Test
    public void WhenFirstPlayerMakeValidPut() {
        mill.makeTurn(firstPlayer, "5");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeValidPut() {
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerPutHisStoneToNotFreePosition() {
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "5");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenIsNotDropTurnAndFirstPlayerDropStone() {
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");

        mill.makeTurn(firstPlayer, "6");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakePutOutOfTurn() {
        mill.makeTurn(secondPlayer, "5");

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeReplaceOutOfTurn() {
        mill.makeTurn(secondPlayer, "5,6");

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerMakeDropOutOfTurn() {
        mill.makeTurn(secondPlayer, "5");

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceStoneWhenHeHasStonesInHand() {
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");

        mill.makeTurn(firstPlayer, "5,7");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMill() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");

        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        GameState expected = GameState.WAIT_FOR_FIRST_PLAYER_DROP;
        GameState actual = mill.getGameState();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndThenHePutStone() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(firstPlayer, "8");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndThenHeReplaceStone() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(firstPlayer, "4,8");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropHisOwnStone() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(firstPlayer, "4");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropEmptyPosition() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(firstPlayer, "12");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndDropOpponentStone() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(firstPlayer, "0");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeMillAndSecondPlayerTryToMakeTurn() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)

        mill.makeTurn(secondPlayer, "4");

        GameResultCode expected = GameResultCode.BAD_TURN_ORDER;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenPlayerDropOpponentStoneInMillWhenThereAreOpponentStoneOutOfMill() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "14");    //out of millEngine (14)
        mill.makeTurn(secondPlayer, "2");    //secondPlayer create millEngine (0 1 2)
        mill.makeTurn(secondPlayer, "1");

        mill.makeTurn(secondPlayer, "4");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerDropOpponentStoneInMillWhenThereAreNotOpponentStoneOutOfMill() {
        mill.makeTurn(firstPlayer, "4");
        mill.makeTurn(secondPlayer, "0");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "1");
        mill.makeTurn(firstPlayer, "6");     //firstPlayer create millEngine (4 5 6)
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "1");
        mill.setGameState(GameState.WAIT_FOR_SECOND_PLAYER_TURN);
        mill.makeTurn(secondPlayer, "2");    //secondPlayer create millEngine (0 1 2)
        mill.makeTurn(secondPlayer, "1");

        mill.makeTurn(secondPlayer, "4");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerPutStoneWhenHisHandEmpty() {
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "2");
        mill.makeTurn(firstPlayer, "3");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "8");
        mill.makeTurn(firstPlayer, "9");
        mill.makeTurn(secondPlayer, "10");
        mill.makeTurn(firstPlayer, "11");
        mill.makeTurn(secondPlayer, "12");
        mill.makeTurn(firstPlayer, "13");
        mill.makeTurn(secondPlayer, "14");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "16");
        mill.makeTurn(firstPlayer, "18");    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "19");   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, "20");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerMakeValidReplace() {
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "2");
        mill.makeTurn(firstPlayer, "3");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "8");
        mill.makeTurn(firstPlayer, "9");
        mill.makeTurn(secondPlayer, "10");
        mill.makeTurn(firstPlayer, "11");
        mill.makeTurn(secondPlayer, "12");
        mill.makeTurn(firstPlayer, "13");
        mill.makeTurn(secondPlayer, "14");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "16");
        mill.makeTurn(firstPlayer, "18");    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "19");   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, "1,0");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);

    }

    @Test
    public void WhenFirstPlayerMakeReplaceToNotFreePosition() {
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "2");
        mill.makeTurn(firstPlayer, "3");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "8");
        mill.makeTurn(firstPlayer, "9");
        mill.makeTurn(secondPlayer, "10");
        mill.makeTurn(firstPlayer, "11");
        mill.makeTurn(secondPlayer, "12");
        mill.makeTurn(firstPlayer, "13");
        mill.makeTurn(secondPlayer, "14");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "16");
        mill.makeTurn(firstPlayer, "18");    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "19");   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, "1,2");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceStoneToNotNeighborPositionWhenHeHasMoreThen3StoneInGame() {
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "2");
        mill.makeTurn(firstPlayer, "3");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "5");
        mill.makeTurn(secondPlayer, "6");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "8");
        mill.makeTurn(firstPlayer, "9");
        mill.makeTurn(secondPlayer, "10");
        mill.makeTurn(firstPlayer, "11");
        mill.makeTurn(secondPlayer, "12");
        mill.makeTurn(firstPlayer, "13");
        mill.makeTurn(secondPlayer, "14");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "16");
        mill.makeTurn(firstPlayer, "18");    // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "19");   // Doesn't have stone in hand

        mill.makeTurn(firstPlayer, "1,22");

        GameResultCode expected = GameResultCode.BAD_TURN_LOGIC;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenSecondPlayerReplaceStoneToNotNeighborPositionWhenHeHasOly3StoneInGame() {
        mill.makeTurn(firstPlayer, "0");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "2");                //firstPlayer create millEngine (0 1 2)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 8 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "3");
        mill.makeTurn(firstPlayer, "6");                //firstPlayer create millEngine (0 7 6)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 7 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "11");
        mill.makeTurn(firstPlayer, "23");               //firstPlayer create millEngine (7 15 23)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 6 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "8");
        mill.makeTurn(secondPlayer, "13");
        mill.makeTurn(firstPlayer, "10");               // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "20");              // Doesn't have stone in hand
        mill.makeTurn(firstPlayer, "1,9");              //firstPlayer create millEngine (8 9 10)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 5 getStonesList;
        mill.makeTurn(secondPlayer, "13,5");
        mill.makeTurn(firstPlayer, "9,1");              //firstPlayer create millEngine (0 1 2)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 4 getStonesList;
        mill.makeTurn(secondPlayer, "4,5");
        mill.makeTurn(firstPlayer, "1,9");              //firstPlayer create millEngine (8 9 10)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 3 getStonesList;

        mill.makeTurn(secondPlayer, "11,5");

        GameResultCode expected = GameResultCode.OK;
        GameResultCode actual = mill.getResultCode();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerReplaceHisStoneAndDestroyMillButCreateNewMill() {
        mill.makeTurn(firstPlayer, "0");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "2");                //firstPlayer create millEngine (0 1 2)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 8 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "3");
        mill.makeTurn(firstPlayer, "6");                //firstPlayer create millEngine (0 7 6)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 7 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "11");
        mill.makeTurn(firstPlayer, "23");               //firstPlayer create millEngine (7 15 23)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 6 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "8");
        mill.makeTurn(secondPlayer, "13");
        mill.makeTurn(firstPlayer, "10");               // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "20");              // Doesn't have stone in hand

        //firstPlayer destroy millEngine (0 1 2) and create millEngine (8 9 10)
        mill.makeTurn(firstPlayer, "1,9");

        GameState expected = GameState.WAIT_FOR_FIRST_PLAYER_DROP;
        GameState actual = mill.getGameState();
        assertSame(expected, actual);
    }

    @Test
    public void WhenFirstPlayerDropOpponentStoneAndOpponentHas2Stones() {
        mill.makeTurn(firstPlayer, "0");
        mill.makeTurn(secondPlayer, "4");
        mill.makeTurn(firstPlayer, "1");
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "2");                //firstPlayer create millEngine (0 1 2)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 8 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "7");
        mill.makeTurn(secondPlayer, "3");
        mill.makeTurn(firstPlayer, "6");                //firstPlayer create millEngine (0 7 6)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 7 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "15");
        mill.makeTurn(secondPlayer, "11");
        mill.makeTurn(firstPlayer, "23");               //firstPlayer create millEngine (7 15 23)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 6 getStonesList;
        mill.makeTurn(secondPlayer, "5");
        mill.makeTurn(firstPlayer, "8");
        mill.makeTurn(secondPlayer, "13");
        mill.makeTurn(firstPlayer, "10");               // Doesn't have stone in hand
        mill.makeTurn(secondPlayer, "20");              // Doesn't have stone in hand
        mill.makeTurn(firstPlayer, "1,9");              //firstPlayer create millEngine (8 9 10)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 5 getStonesList;
        mill.makeTurn(secondPlayer, "13,5");
        mill.makeTurn(firstPlayer, "9,1");              //firstPlayer create millEngine (0 1 2)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 4 getStonesList;
        mill.makeTurn(secondPlayer, "4,5");
        mill.makeTurn(firstPlayer, "1,9");              //firstPlayer create millEngine (8 9 10)
        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 3 getStonesList;
        mill.makeTurn(secondPlayer, "11,5");
        mill.makeTurn(firstPlayer, "9,1");              //firstPlayer create millEngine (0 1 2)

        mill.makeTurn(firstPlayer, "5");                 //secondPlayer has 2 getStonesList;

        GameState expected = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        GameState actual = mill.getGameState();
        assertSame(expected, actual);
    }
}
