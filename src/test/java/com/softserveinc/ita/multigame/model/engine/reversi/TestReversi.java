package com.softserveinc.ita.multigame.model.engine.reversi;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameState;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link com.softserveinc.ita.multigame.model.engine.reversi.Reversi}
 *
 * @author Igor Khlaponin igor.boxmails@gmail.com
 */

public class TestReversi {

    private Reversi reversi;

    @Mock
    private Player player1;
    @Mock
    private Player player2;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(player1.getLogin()).thenReturn("Sam");
        when(player2.getLogin()).thenReturn("Frodo");

        reversi = new Reversi();
    }

    @Test
    public void checkGetGameBoard() {
        assertNotNull(reversi.getGameBoard());
    }

    @Test
    public void checkGetBoardInfo() {
        Object expected = ReversiUtil.arrayToCellsListParser(reversi.getGameBoard().getField());
        Object actual = reversi.getBoard();
        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void checkGetCurrentPlayer() {
        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);

        Player expected = reversi.getFirstPlayer();
        Player actual = reversi.getCurrentPlayer();

        assertEquals(expected, actual);
    }

    @Test
    public void checkIfWeCouldMakeTurn() {
        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        assertEquals(GameState.WAIT_FOR_FIRST_PLAYER_TURN, reversi.getCurrentGameState());
        assertTrue(reversi.makeTurn(player1, "f4"));
    }

    @Test
    public void checkIfGameIsFinishedAfterLastTurn() {
        int[][] field = new int[8][8];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1 + (int) (Math.random() * 2);
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1, "a8");

        assertTrue(reversi.isFinished());
    }

    @Test
    public void checkIfGameIsFinishedAfterWithFirstPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1, "a8");
        GameState gameState = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        assertEquals(gameState, reversi.getCurrentGameState());
    }

    @Test
    public void checkIfGameIsFinishedAfterWithSecondPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 2;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1, "a8");
        GameState gameState = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        assertEquals(gameState, reversi.getCurrentGameState());
    }

    @Test
    public void checkTheFirstPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 1;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        assertFalse(reversi.isFinished());

        reversi.getGameBoard().setField(field);
        reversi.makeTurn(player1, "a8");
        assertEquals(player1, reversi.getTheWinner());
    }

    @Test
    public void checkTheSecondPlayerAsAWinner() {
        int[][] field = new int[8][8];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = 2;
            }
        }
        field[0][0] = 0;

        reversi.setFirstPlayer(player1);
        reversi.setSecondPlayer(player2);
        reversi.getGameBoard().setField(field);

        reversi.makeTurn(player1, "a8");
        assertEquals(player2, reversi.getTheWinner());
    }

}
