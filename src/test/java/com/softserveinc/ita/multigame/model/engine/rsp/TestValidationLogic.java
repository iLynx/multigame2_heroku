package com.softserveinc.ita.multigame.model.engine.rsp;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Testing RSP game engine
 *
 * @author Andrew Volyk andredewoll@gmail.com avolyk1tc
 */

public class TestValidationLogic {

    @Test
    public void MockTestingValidationTurnLogic() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper();

        assertTrue(rockScissorsPaper.validateTurnLogic("SPOCK"));
    }
}
